from __future__ import annotations

from abc import ABC

class Person(ABC):
        def __init__(self, firstName: str, lastName: str, email: str, department: str) -> None:
            self._firstName: str = firstName
            self._lastName: str = lastName
            self._email: str = email
            self._department: str = department
        ###############################################################
        
        def getFirstName(self) -> None:
            print(f"{self._firstName}")

        def getLastName(self) -> None:
            print(f"{self._lastName}")

        def getFullName(self) -> str:
            return f"{self._firstName} {self._lastName}"

        ###############################################################

        def setFirstName(self, new_firstName: str) -> None:
            self._firstName = new_firstName

        def setLastName(self, new_lastName: str) -> None:
            self._lastName = new_lastName

        ###############################################################

        def addRequest(self) -> str:
            return "Request has been added"

        def checkRequest(self) -> None:
            pass

        def addUser(self) -> str: # type: ignore
            pass

        def login(self) -> str:
            return f"{self._email} has logged in"

        def logout(self) -> str:
            return f"{self._email} has logged out"


class Employee(Person):
        def __init__(self, firstName: str, lastName: str, email: str, department: str) -> None:
            super().__init__(firstName, lastName, email, department)

        def checkRequest(self) -> None:
            pass 
        
        def addUser(self) -> str: # type: ignore
            pass


class TeamLead(Person):
        def __init__(self, firstName: str, lastName: str, email: str, department: str) -> None:
            super().__init__(firstName, lastName, email, department)

            self._members: list[Employee] = []
        
        def checkRequest(self) -> None:
            pass 
        
        def addUser(self) -> str: # type: ignore
            pass

        def addMember(self, member: Employee) -> None:
            self._members.append(member)

        ###############################################################

        def get_members(self) -> list[Employee]:
            return self._members

class Admin(Person):
        def __init__(self, firstName: str, lastName: str, email: str, department: str) -> None:
            super().__init__(firstName, lastName, email, department)
        
        def checkRequest(self) -> None:
            pass 
        
        ###############################################################

        def addUser(self) -> str:
            return "User has been added"


class Request:
        def __init__(self, name: str, requester: TeamLead | Employee, dateRequested: str) -> None:
            self._name: str = name
            self._requester: TeamLead | Employee = requester
            self._dateRequested: str = dateRequested
            self._status: str = "Open"

        def updateRequest(self) -> None:
            pass

        def closeRequest(self) -> None:
            pass
        
        def cancelRequest(self) -> None:
            pass

        ###############################################################
        
        def set_status(self, new_status: str) -> None:
            self._status = new_status
        


employee1: Employee = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2: Employee = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3: Employee = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4: Employee = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1: Admin = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1: TeamLead = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1: Request = Request("New hire orientation", teamLead1, "27-Jul-2021") 
req2: Request = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())


